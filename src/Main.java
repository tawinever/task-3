import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        String year;
        String month;
        Integer validYear = null;
        Integer validMonth = null;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        while (validYear == null) {
            try {
                System.out.println("Enter year:");
                year = bufferedReader.readLine();
                validYear = isIntegerInInterval(year, 0, Integer.MAX_VALUE);
                if (validYear == null) {
                    System.out.println("Invalid year, try again.");
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Invalid year, try again.");
            }
        }

        while (validMonth == null) {
            try {
                System.out.println("Enter month:");
                month = bufferedReader.readLine();
                validMonth = isIntegerInInterval(month, 1, 12);
                if (validMonth == null) {
                    System.out.println("Invalid month, try again.");
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Invalid month, try again.");
            }
        }

        System.out.println(validYear + " " + getMonthName(validMonth) + " - " + isLeapYearString(validYear));

        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Integer isIntegerInInterval(String number, int leftInterval, int rightInterval) {
        int convertedNumber;
        try {
            convertedNumber = Integer.parseInt(number.trim());
        } catch (Exception e) {
            return null;
        }

        if (convertedNumber < leftInterval || convertedNumber > rightInterval) {
            return null;
        }

        return convertedNumber;
    }

    public static String isLeapYearString(int year) {
        if (isLeapYear(year))
            return "leap year";
        else
            return "not leap year";
    }

    public static boolean isLeapYear(int year) {
        GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
        return gregorianCalendar.isLeapYear(year);
    }

    public static String getMonthName(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }
}
